import App from './App'
import store from './store'
import '.env.js'
// 引入全局uView
import uView from '@/uni_modules/uview-ui'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
Vue.prototype.$store = store
App.mpType = 'app'
// main.js，注意要在use方法之后执行
Vue.use(uView)
const app = new Vue({
	store,
	...App
})
app.$mount()
// #endif


// #ifdef VUE3
import {createSSRApp} from 'vue'
// main.js，注意要在use方法之后执行

export function createApp() {
	const app = createSSRApp(App)
	app.use(store)
	return {app}
}
// #endif
