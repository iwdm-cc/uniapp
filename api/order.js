/*
	@author 张成
	@description 订单相关操作
 */
const request = require("../utils/request")
import {
	showToast,
	setStorage
} from "../utils/util";

//下单
export function addOrder() {
	return new Promise((resolve, reject) => {
		request.requestAuth("/express/order/addOrder", "post")
			.then((res) => {
				let data = res.data;
				if (data.code == 200) {
					resolve(data);
				} else {
					reject(data);
				}
			})
			.catch((err) => {
				reject(err);
			});
	});

}

//查询自己的订单
export function queryOwnOrder(data={status:0}) {
	return new Promise((resolve, reject) => {
		request.requestAuth("/express/order/queryOwnOrder", "get",data)
			.then((res) => {
				let data = res.data;
				if (data.code == 200) {
					resolve(data);
				} else {
					reject(data);
				}
			})
			.catch((err) => {
				reject(err);
			});
	});

}
