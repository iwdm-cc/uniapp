/*
	@author 张成
	@description 签到相关操作
 */
const request = require("../utils/request")
import {
	showToast,
	setStorage
} from "../utils/util";

//签到
export function signIn() {
	return new Promise((resolve, reject) => {
		request.requestAuth("/user/sign/in", "get")
			.then((res) => {
				let data = res.data;
				if (data.code == 200) {
					resolve(data);
				} else {
					reject(data);
				}
			})
			.catch((err) => {
				reject(err);
			});
	});

}

//签到页面获得列表
export function signIn() {
	return new Promise((resolve, reject) => {
		request.requestAuth("/user/sign/list", "get")
			.then((res) => {
				let data = res.data;
				if (data.code == 200) {
					resolve(data);
				} else {
					reject(data);
				}
			})
			.catch((err) => {
				reject(err);
			});
	});

}
//将点券兑换为积分
export function signIn() {
	return new Promise((resolve, reject) => {
		request.requestAuth("/user/sign/change", "get")
			.then((res) => {
				let data = res.data;
				if (data.code == 200) {
					resolve(data);
				} else {
					reject(data);
				}
			})
			.catch((err) => {
				reject(err);
			});
	});

}
