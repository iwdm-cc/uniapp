/*
	@author 张成
	@description 用户相关操作
 */
const request = require("../utils/request")
import {
	showToast,
	setStorage
} from "../utils/util";

//获取用户信息
export function getUserInfo() {
	return new Promise((resolve, reject) => {
		request.requestAuth("/user/info", "get")
			.then((res) => {
				let data = res.data;
				if (data.code == 200) {
					resolve(data);
				} else {
					reject(data);
				}
			})
			.catch((err) => {
				reject(err);
			});
	});
	
}

//验证 token
export function checkToken() {
	return new Promise((resolve, reject) => {
		request.requestAuth("/user/checkToken", "get")
			.then((res) => {
				let data = res.data;
				if (data.code == 200) {
					resolve(data);
				} else {
					reject(data);
				}
			})
			.catch((err) => {
				reject(err);
			});
	});
	
}
//登录
export function loginT(code, platform) {
	return uniCloud.callFunction({
		name: 'CloudLogin',
		data: {
			"JSCODE": code,
			"provider": platform
		}
	})
}

//获取Token by openId
export function OpenidLogin(userInfo) {
	userInfo.active = request.Active
	return new Promise((resolve, reject) => {

		request.requestBase("/user/OpenidLogin", "get", userInfo)
			.then((res) => {
				let data = res.data;
				if (data.code == 200) {
					let token = data.data.token;
					setStorage("token", token);
					resolve(data);
				} else {
					reject(data);
				}
			})
			.catch((err) => {
				reject(err);
			});
	});

}
