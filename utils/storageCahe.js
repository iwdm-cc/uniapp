const cache_key = "_cache_time"

/**
 * 设置
 * k 键key
 * v 值value
 * t 秒
 */
function cache_put(k, v, t) {
    wx.setStorageSync(k, v)
    var seconds = parseInt(t)
    if (seconds > 0) {
        var newtime = Date.parse(new Date())
        newtime = newtime / 1000 + seconds;
        wx.setStorageSync(k + cache_key, newtime + "")
    } else {
        wx.removeStorageSync(k + cache_key)
    }
}
/**
 * 获取
 * k 键key
 */
function cache_get(k) {
    var deadtime = parseInt(wx.getStorageSync(k + cache_key))
    if (deadtime) {
        if (parseInt(deadtime) < (Date.parse(new Date()) / 1000)) {
            wx.removeStorageSync(k);
            return null
        }
    }
    var res = wx.getStorageSync(k)
    if (res) {
        return res
    } else {
        return null
    }
}

/**
 * 删除
 */
function cache_remove(k) {
    wx.removeStorageSync(k);
    wx.removeStorageSync(k + cache_key);
}

/**
 * 清除所有key
 */
function cache_clear() {
    wx.clearStorageSync();
}
export default {
    cache_put,
    cache_get,
    cache_remove,
    cache_clear
}